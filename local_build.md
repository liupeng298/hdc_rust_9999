# 使用OH构建系统在蓝区本地构建

## Step 1. 获取 pty-process 依赖库
### 方法一 使用 cargo 编译 master 分支

    cd hdc_rust

    OH_DIR='/path/to/OpenHarmony' cargo build

编译完成后 cargo 的 home 目录下会有项目所有依赖的源码，cargo home 的路径一般为 `~/.cargo`，pty-process 的源码在 `~/.cargo/registry/src/xxxxxxx/pty-process-0.3.0/`

### 方法二 自行下载

`https://docs.rs/crate/pty-process/0.3.0/source/`

## Step 2. pty-process 放入本地 OH

### 加入 pty-process 的 gn 文件
    cp /path/to/hdc_rust/BUILD.gn.pty-process /path/to/pty-process-0.3.0
### pty-process 放进 OH
    cp -r /path/to/pty-process-0.3.0 /path/to/OpenHarmony/third_party/rust/crates/pty-process

## Step 3. hdc_rust 放入本地 OH 并构建

hdc_rust 放入本地 OH 中的 hdc 目录下

    cp -r /path/to/hdc_rust /path/to/OpenHarmony/developtools/hdc

进入 OH 的 hdc 目录

    cd /path/to/OpenHarmony/developtools/hdc

备份 bundle.json

    mv bundle.json bundle.json.bak

替换 bundle.json

    cp hdc_rust/bundle.json.hdc bundle.json

编译rk镜像，烧写

## Step 4. 启动

通过 hdc host 连接rk，启动 hdc_rust daemon 端

    hdc shell /system/bin/hdc_rust

默认开启60000端口监听，此时再开一个命令行连接上

    hdc tconn <ip>:60000

测试收发文件

    hdc -t <ip>:60000 file send <somefile>
    hdc -t <ip>:60000 file recv <somefile>
