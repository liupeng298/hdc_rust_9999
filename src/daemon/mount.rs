extern "C" {
    fn RemountDeviceEx() -> bool;
}

pub fn remount_device() -> bool {
    unsafe { RemountDeviceEx() }
}
