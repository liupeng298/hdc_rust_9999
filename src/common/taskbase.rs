//! taskbase
#![allow(missing_docs)]

use crate::config::HdcCommand;

pub trait TaskBase: Send + Sync + 'static {
    fn command_dispatch(
        &mut self,
        _command: HdcCommand,
        _payload: &[u8],
        _payload_size: u16,
    ) -> bool;
    fn stop_task(&mut self);
    fn ready_for_release(&mut self) -> bool;
    fn channel_id(&self) -> u32 {
        0
    }
    fn task_finish(&self);
}
