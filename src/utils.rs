//! utils
#![allow(missing_docs)]

use crate::config::SHELL_PROG;

use std::io::{self, Error, ErrorKind};
use std::process::Command;
use std::time::{SystemTime, UNIX_EPOCH};

use ylong_runtime::io::AsyncWriteExt;

pub fn get_pseudo_random_u32() -> u32 {
    (SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos()
        & 0xffffffff) as u32
}

pub fn get_current_time() -> u64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis() as u64
}

pub async fn print_msg(buf: Vec<u8>) -> io::Result<()> {
    let mut stdout = ylong_runtime::io::stdout();
    let _ = stdout.write(&buf).await;
    let _ = stdout.flush().await;
    Ok(())
}

pub fn execute_cmd(cmd: String) -> Vec<u8> {
    let arg_sign = if cfg!(target_os = "windows") {
        "/c"
    } else {
        "-c"
    };
    let result = Command::new(SHELL_PROG).args([arg_sign, &cmd]).output();

    match result {
        Ok(output) => [output.stdout, output.stderr].concat(),
        Err(e) => e.to_string().into_bytes(),
    }
}

pub fn error_other(msg: String) -> Error {
    Error::new(ErrorKind::Other, msg)
}

pub mod hdc_log {
    #[cfg(feature = "hdc_daemon")]
    pub use hilog_rust::{hilog, HiLogLabel, LogType};
    #[cfg(feature = "hdc_daemon")]
    pub use std::ffi::{c_char, CString};

    #[cfg(feature = "hdc_daemon")]
    pub const LOG_LABEL: HiLogLabel = HiLogLabel {
        log_type: LogType::LogCore,
        domain: 0xD002D13,
        tag: "HDC_LOG",
    };

    #[cfg(feature = "hdc_daemon")]
    #[macro_export]
    macro_rules! trace {
        ($($arg:tt)+) => {
            let head = format!("{}:{}", file!().split('/').last().unwrap(), line!());
            hilog_rust::info!(LOG_LABEL, "{} {}", @public(head), @public(format!($($arg)+)));
            log::trace!($($arg)+);
        };
    }

    #[cfg(not(feature = "hdc_daemon"))]
    #[macro_export]
    macro_rules! trace {
        ($($arg:tt)+) => {
            log::trace!($($arg)+);
        };
    }

    #[cfg(feature = "hdc_daemon")]
    #[macro_export]
    macro_rules! debug {
        ($($arg:tt)+) => {
            let head = format!("{}:{}", file!().split('/').last().unwrap(), line!());
            hilog_rust::info!(LOG_LABEL, "{} {}", @public(head), @public(format!($($arg)+)));
            log::debug!($($arg)+);
        };
    }

    #[cfg(not(feature = "hdc_daemon"))]
    #[macro_export]
    macro_rules! debug {
        ($($arg:tt)+) => {
            log::debug!($($arg)+);
        };
    }

    #[cfg(feature = "hdc_daemon")]
    #[macro_export]
    macro_rules! info {
        ($($arg:tt)+) => {
            let head = format!("{}:{}", file!().split('/').last().unwrap(), line!());
            hilog_rust::info!(LOG_LABEL, "{} {}", @public(head), @public(format!($($arg)+)));
            log::info!($($arg)+);
        };
    }

    #[cfg(not(feature = "hdc_daemon"))]
    #[macro_export]
    macro_rules! info {
        ($($arg:tt)+) => {
            log::info!($($arg)+);
        };
    }

    #[cfg(feature = "hdc_daemon")]
    #[macro_export]
    macro_rules! warn {
        ($($arg:tt)+) => {
            let head = format!("{}:{}", file!().split('/').last().unwrap(), line!());
            hilog_rust::warn!(LOG_LABEL, "{} {}", @public(head), @public(format!($($arg)+)));
            log::warn!($($arg)+);
        };
    }

    #[cfg(not(feature = "hdc_daemon"))]
    #[macro_export]
    macro_rules! warn {
        ($($arg:tt)+) => {
            log::warn!($($arg)+);
        };
    }

    #[cfg(feature = "hdc_daemon")]
    #[macro_export]
    macro_rules! error {
        ($($arg:tt)+) => {
            let head = format!("{}:{}", file!().split('/').last().unwrap(), line!());
            hilog_rust::error!(LOG_LABEL, "{} {}", @public(head), @public(format!($($arg)+)));
            log::error!($($arg)+);
        };
    }

    #[cfg(not(feature = "hdc_daemon"))]
    #[macro_export]
    macro_rules! error {
        ($($arg:tt)+) => {
            log::error!($($arg)+);
        };
    }

    #[cfg(feature = "hdc_daemon")]
    #[macro_export]
    macro_rules! fatal {
        ($($arg:tt)+) => {
            let head = format!("{}:{}", file!().split('/').last().unwrap(), line!());
            hilog_rust::fatal!(LOG_LABEL, "{} {}", @public(head), @public(format!($($arg)+)));
            log::fatal!($($arg)+);
        };
    }

    #[cfg(not(feature = "hdc_daemon"))]
    #[macro_export]
    macro_rules! fatal {
        ($($arg:tt)+) => {
            log::fatal!($($arg)+);
        };
    }

    // pub use crate::{trace, debug, info, warn, error, fatal};
}
