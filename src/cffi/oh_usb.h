#ifndef USB_COMMON_H
#define USB_COMMON_H

#include <vector>
#include <string>

#include "usb_types.h"
#include "usb_ffs.h"

void FillUsbV2Head(struct Hdc::usb_functionfs_desc_v2 &descUsbFfs);

int ConfigEpPoint(int& controlEp, const std::string& path);

int OpenEpPoint(int &fd, const std::string path);

int CloseUsbFd(int &fd);

void CloseEndpoint(int &bulkInFd, int &bulkOutFd, int &controlEp, bool closeCtrlEp);

int WriteData(int bulkIn, const uint8_t *data, const int length);

int ReadData(int bulkOut, uint8_t* buf, const int readMaxSize);

#endif
