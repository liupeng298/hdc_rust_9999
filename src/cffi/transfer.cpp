#include <lz4.h>

namespace Hdc {

extern "C" int LZ4_compress_transfer(const char* data, char* data_compress, int data_size, int compress_capacity) {
    return LZ4_compress_default((const char *)data, (char *)data_compress,
                                                    data_size, compress_capacity);
}

extern "C" int LZ4_decompress_transfer(const char* data, char* data_decompress, int data_size, int decompress_capacity) {

    return LZ4_decompress_safe((const char *)data, (char *)data_decompress,
                                                    data_size, decompress_capacity);
}

}