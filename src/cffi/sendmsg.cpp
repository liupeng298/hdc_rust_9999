#include "sendmsg.h"

#include <sys/socket.h>
#include <string.h>
#include <alloca.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

namespace Hdc {

extern "C" int SendMsg_(int socket_fd, int fd, char* data, int size) {
    struct iovec iov;
    iov.iov_base = data;
    iov.iov_len = size;
    struct msghdr msg;
    msg.msg_name = nullptr;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    int len = CMSG_SPACE(static_cast<unsigned int>(sizeof(int)));
    char ctlBuf[len];
    msg.msg_control = ctlBuf;
    msg.msg_controllen = sizeof(ctlBuf);

    struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
    if (cmsg == nullptr) {
        printf("SendFdToApp cmsg is nullptr\n");
        return -5;
    }
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(int));
    int result = -1;
    if (memcpy(CMSG_DATA(cmsg), &fd, sizeof(int)) == nullptr) {
        printf("SendFdToApp memcpy error:%d\n", errno);
        return -2;
    }
    if ((result = sendmsg(socket_fd, &msg, 0)) < 0) {
        printf("SendFdToApp sendmsg errno:%d, result:%d\n", errno, result);
        return result;
    }
    printf("send msg ok\n");
    return result;
}
}