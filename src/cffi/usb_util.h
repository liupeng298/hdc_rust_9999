#ifndef USB_UTIL_H
#define USB_UTIL_H

#include <vector>
#include <string>
#include <sys/types.h>
#include <dirent.h>
#include "usb_types.h"
#include <arpa/inet.h>

inline int GetMaxBufSize()
{
    return MAX_SIZE_IOBUF;
}

std::string GetDevPath(const std::string &path);

std::vector<uint8_t> BuildPacketHeader(uint32_t sessionId, uint8_t option, uint32_t dataSize);

const std::string StringFormat(const char * const formater, va_list &vaArgs);

const std::string StringFormat(const char * const formater, ...);

bool RunPipeComand(const char *cmdString, char *outBuf, uint16_t sizeOutBuf, bool ignoreTailLf);

bool SetDevItem(const char *key, const char *value);
#endif
