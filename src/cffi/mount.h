#ifndef FFI_TARGET_MOUNT_H
#define FFI_TARGET_MOUNT_H

#define BUF_SIZE_DEFAULT2 2048
#define BUF_SIZE_SMALL 256

bool FindMountDeviceByPath(const char *toQuery, char *dev);
bool RemountPartition(const char *dir);
bool RemountDevice();

#endif