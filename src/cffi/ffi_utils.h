#ifndef HDC_FFI_UTILS_H
#define HDC_FFI_UTILS_H

#include <string>
#include <securec.h>

namespace Hdc {
struct SerializedBuffer {
    char *ptr;
    uint64_t size;
};

}

#endif
